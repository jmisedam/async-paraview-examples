#!/bin/sh

readonly shatool="sha512sum"
readonly sha512sum="e6de2c81f7cfda82cdcad65da1a7a5c3817e8c6ced1366e95ab1593cc529e207ff33a9da3b1e4e280a5c4b6c4d5821a9167b90c5f9e8dcc1e07c64f0e1cfe594"
readonly filename="rock.vti"
curl "https://www.paraview.org/files/ExternalData/SHA512/${sha512sum}" --output ${filename}

echo "$sha512sum  $filename" > data.sha512sum
$shatool --check data.sha512sum

mkdir -p /opt/paraview/data/Testing/Data
mv rock.vti /opt/paraview/data/Testing/Data/

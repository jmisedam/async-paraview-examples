#!/bin/sh

export LD_LIBRARY_PATH="/usr/lib/x86_64-linux-gnu:${PARAVIEW_RUNTIME_DIR}/lib"
export PATH="${PATH}:${PARAVIEW_RUNTIME_DIR}/bin"

pvpython \
  $@ \
  -D ${PARAVIEW_DATA_DIR} \
  --server \
  --host 127.0.0.1 \
  --venv ${PARAVIEW_VENV_DIR}

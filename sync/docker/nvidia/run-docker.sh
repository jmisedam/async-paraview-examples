#!/usr/bin/env bash
#
# ./run-docker.sh <python script> <script arguments>
# Ex: ./run-docker.sh ./sync/python/rock.py --port 8080
#

docker run \
  -it \
  --gpus all \
  --network="host" \
  -e NVIDIA_DRIVER_CAPABILITIES=all \
  -w "/root" \
  -v "$(pwd):/root/" \
  paraview-sync-run:latest $@

ARG BASE_IMAGE=nvidia/opengl:1.2-glvnd-runtime-ubuntu20.04
ARG DEV_IMAGE=paraview-async-dev
ARG NV_IMAGE=nvidia/opengl:1.2-glvnd-devel-ubuntu20.04

FROM ${DEV_IMAGE} AS devimage
FROM ${NV_IMAGE} AS nv_image
FROM ${BASE_IMAGE}

LABEL maintainer="Jaswant Panchumarti <jaswant.panchumarti@kitware.com>"

ENV TZ "America/New_York"

# -----------------------------------------------------------------------------
# System package
# -----------------------------------------------------------------------------
COPY docker/scripts/install_runtime_dependencies.sh /opt/install_runtime_dependencies.sh
RUN /opt/install_runtime_dependencies.sh

COPY docker/scripts/setup_trame.sh /opt/setup_trame.sh
RUN /opt/setup_trame.sh

# Populate paraview async runtime, data.
COPY --from=devimage  /opt/scripts                           /opt/scripts
# Preserve spack directory structure, but do not copy sources, the `rpath` of libvtkServicesThallium contains `/opt/spack/src/opt/spack` entries.
COPY --from=devimage  /opt/spack/src/opt/spack               /opt/spack/src/opt/spack
COPY --from=devimage  /opt/paraview-async/build/ExternalData /opt/paraview-async/data
COPY --from=devimage  /opt/paraview-async/install            /opt/paraview-async/runtime

# Put them at a place runtime can find.
COPY --from=nv_image /usr/lib/x86_64-linux-gnu/libOpenGL.so.0.0.0  /usr/lib/x86_64-linux-gnu/libOpenGL.so.0.0.0
COPY --from=nv_image /usr/lib/x86_64-linux-gnu/libOpenGL.so.0      /usr/lib/x86_64-linux-gnu/libOpenGL.so.0
COPY --from=nv_image /usr/lib/x86_64-linux-gnu/libOpenGL.so        /usr/lib/x86_64-linux-gnu/libOpenGL.so

# Copy the server script.
COPY docker/scripts/server.sh /opt/scripts/server.sh

ENV PARAVIEW_ASYNC_DATA_DIR="/opt/paraview-async/data"
ENV PARAVIEW_ASYNC_RUNTIME_DIR="/opt/paraview-async/runtime"
ENV PARAVIEW_ASYNC_VENV_DIR="/opt/paraview-async/runtime/.pvenv"

# create non-root user
RUN useradd -ms /bin/bash  paraview-user
RUN chown -R paraview-user /opt
USER paraview-user


ENTRYPOINT [ "/bin/bash", "/opt/scripts/server.sh" ]

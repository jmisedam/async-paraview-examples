#!/bin/sh

set -e


readonly version="1.10.2"

readonly shatool="sha256sum"
readonly sha256sum="763464859c7ef2ea3a0a10f4df40d2025d3bb9438fcb1228404640410c0ec22d"
readonly platform="linux"

readonly filename="ninja-$platform"
readonly tarball="$filename.zip"

echo "$sha256sum  $tarball" > ninja.sha256sum
curl -OL "https://github.com/ninja-build/ninja/releases/download/v$version/$tarball"
$shatool --check ninja.sha256sum
./cmake/bin/cmake -E tar xf "$tarball"

mkdir -p /opt/bin
mv ninja /opt/bin/

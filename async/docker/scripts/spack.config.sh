#!/bin/sh

set -e
# Inject distro build tools to avoid unnecessary builds from spack.
mkdir -p paraview-async/spack-config
cat > "paraview-async/spack-config/packages.yaml" <<EOF
packages:
  all:
    target: [x86_64]
  autoconf:
    buildable: false
    externals:
    - spec: autoconf@2.69
      prefix: /usr
  automake:
    buildable: false
    externals:
    - spec: automake@1.16.1
      prefix: /usr
  boost:
    buildable: false
    externals:
    - spec: boost@1.71.0
      prefix: /usr
  cereal:
    buildable: false
    externals:
    - spec: cereal@1.3.0
      prefix: /usr
  cmake:
    buildable: false
    externals:
    - spec: cmake@$( cmake --version | head -n1 | cut -d' ' -f3 )
      prefix: /opt/cmake
  json-c:
    buildable: false
    externals:
    - spec: json-c@0.13.1
      prefix: /usr
  libtool:
    buildable: false
    externals:
    - spec: libtool@2.4.6
      prefix: /usr
  libsigsegv:
    buildable: false
    externals:
    - spec: libsigsegv@2.12
      prefix: /usr
  m4:
    buildable: false
    externals:
    - spec: m4@2.14.02-1
      prefix: /usr
  perl:
    buildable: false
    externals:
    - spec: perl@5.30.0
      prefix: /usr
  pkgconf:
    buildable: false
    externals:
    - spec: pkgconf@1.6.3
      prefix: /usr
EOF

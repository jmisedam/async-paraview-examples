#!/bin/sh

set -e

readonly version="3.21.2"

readonly shatool="sha256sum"
readonly sha256sum="d5517d949eaa8f10a149ca250e811e1473ee3f6f10935f1f69596a1e184eafc1"
readonly platform="linux-x86_64"

readonly filename="cmake-$version-$platform"
readonly tarball="$filename.tar.gz"

echo "$sha256sum  $tarball" > cmake.sha256sum
curl -OL "https://github.com/Kitware/CMake/releases/download/v$version/$tarball"
$shatool --check cmake.sha256sum
tar xf "$tarball"
mv "$filename" cmake

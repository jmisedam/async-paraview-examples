#!/bin/sh

# setup apython virtual environment with trame
set -e

python3 -m venv /opt/paraview-async/runtime/.pvenv

. /opt/paraview-async/runtime/.pvenv/bin/activate

pip install -U pip
pip install trame

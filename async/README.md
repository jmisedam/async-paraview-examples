# Running the reference examples with the Asynchronous ParaView backend

The runtime environment for the Asynchronous backend can be setup either by
using a pre-build docker container for Linux, creating a docker container locally or compiling the code from source.

## Pre-built docker image

```shell
docker pull kitware/paraview:async-demo-SC22
```

To run any of the reference applications use
```shell
./async/docker/nvidia/run-docker.sh <python script> <script arguments>
```

Based on the current reference examples you can run the following commands:
```shell
./async/docker/nvidia/run-docker.sh ./async/python/wavelet.py
./async/docker/nvidia/run-docker.sh ./async/python/rock.py
```


### Building and running from source

1. Compile the project by executing [async/paraview/build.sh](https://gitlab.kitware.com/async/paraview/-/blob/master/build.sh).
    ```shell
    git clone --recursive https://gitlab.kitware.com/async/paraview.git
    cd paraview
    ./build.sh v0.9.0
    ```
2. Create a virtual environment and activate it. Additionally, upgrade pip and install trame.
    ```shell
    python -m venv pv-env
    source ./pv-env/bin/activate
    pip install -U pip
    pip install trame
    ```

To run the any of the reference applications use
```shell
$PWD/work/build/bin/pvpython  <python script> <script arguments> --venv pv-env
```

Based on the current reference examples you can run the following commands:
- `$PWD/work/build/bin/pvpython ./python/rock.py -D $PWD/work/build/ExternalData/Testing/Data` --venv pv-env
- `$PWD/work/build/bin/pvpython ./python/wavelet.py` --venv pv-env

## Tips
When running multiple containers, try to use different port numbers, 8080,
8081, etc. by passing `--port 8080` as a script argument.

# -----------------------------------------------------------------------------
# export PYTHONPATH=/path/to/async/paraview/build/lib/pythonx.y/site-packages
# export LD_LIBRARY_PATH=/path/to/async/paraview/build/lib
#
# python3.10 -m venv pv-venv
# source ./pv-venv/bin/activate
# pip install -U pip
# pip install -U trame
#
# python ./wavelet.py
# or using pvpython:
# pvpython ./wavelet.py --venv ./pv-venv
# -----------------------------------------------------------------------------

from vtkmodules.vtkCommonCore import vtkLogger
import parat.venv
import asyncio
import json
from parat.services import (
    ApplyController,
    ParaT,
    PipelineBuilder,
    DefinitionManager,
    ActiveObjects,
    PropertyManager,
    PipelineViewer,
    ProgressObserver,
)
from paraview.modules.vtkRemotingServerManager import vtkSMProxySelectionModel
from parat.trame.rca import ViewAdapter

from trame.app import get_server, asynchronous
from trame.ui.vuetify import SinglePageWithDrawerLayout
from trame.widgets import vuetify, trame, rca, html

# -----------------------------------------------------------------------------
# Global helpers
# -----------------------------------------------------------------------------

vtkLogger.SetStderrVerbosity(vtkLogger.VERBOSITY_WARNING)

WAVELET_SCALAR_RANGE = [37.35310363769531, 276.8288269042969]
BITRATE_UNITS = ["B/s", "KB/s", "MB/s"]
INITIAL_STREAM_BITRATE = 12000
INITIAL_STILL_STREAM_QUALITY = 90
INITIAL_INTERACTIVE_STREAM_QUALITY = 60
INITIAL_TARGET_FPS = 30 # suggested for jpeg.


def generate_contour_values(data_range, number_of_contours):
    delta = (data_range[1] - data_range[0]) / (number_of_contours - 1)
    return [data_range[0] + (delta * float(i)) for i in range(number_of_contours)]


def scale_data_rate_to_bytes(value_in_bits):
    return value_in_bits / 8


# value must be in bits/sec
def get_human_readable_stream_bitrate(value_in_bps):
    value = scale_data_rate_to_bytes(value_in_bps)
    for i, units in enumerate(BITRATE_UNITS):
        if value < 1000:
            return f"{value} {units}"
        else:
            value /= 1000


# -----------------------------------------------------------------------------
# Core Application
# -----------------------------------------------------------------------------
class App:
    """This class demonstrates how a developer can leverage python 
    asyncio, trame and the async paraview library to build a paraview
    pipeline in a responsive web application. The python code is
    centered around Microservices that let you achieve simple tasks
    which were very verbose in synchronous ParaView i.e, paraview as we know it today
    
    Highlights
    ----------

    1. initialize `vtkPVPythonApplication` with an instance of `ParaT` from the async paraview library.
    2. create sources, filters, views and representations with the `PipelineBuilder` microservice.
    3. configure proxy properties with `PropertyManager` microservice.
    4. monitor render/data service progress with `ProgressObserver` microservice.
    5. get notified of changes in a pipeline with `PipelineViewer` microservice.
    6. get notified of apply events with `ApplyController` microservice.
    7. update a paraview pipeline and view.
    8. listen to a stream of images from a view.
    9. invoke an interactive/still render on a view at a controlled rate.

    Properties:
    ----------

    There is a distinct pattern in this code. Almost every usage of
    `PropertyManager.SetValue` is followed by `ApplyController.Apply`
    and another strikingly important fact to note is that all usages of
    the `PropertyManager` microservice use `force_push=true`. It's role
    is to push those properties to the server side object. Alternatively,
    the properties can be pushed all at once for one or more proxies using
    `Push(proxy0,proxy2...)` method of property manager. The `ApplyController.Apply`
    method shall update the pipeline with these new properties.
    """
    def __init__(self, server=None):
        # Boilerplate code for trame applications.
        if server is None:
            server = get_server()

        self.server = server
        self.state = server.state
        self.ctrl = server.controller

        # page title
        self.state.trame__title = "Async ParaView"

        # `ParaT` must be instantiated in all python applications that use async paraview library.
        # It initializes a `vtkPVApplication` with an event loop obtained from `asyncio`` so that
        # your python application can wait on futures while processing other application events
        # such as `CreateProxy()`, `CreateFilter()`, `CreateView()`, `CreateRepresentation()`,
        # `UpdatePipeline()`, `view.Update()`.
        # Further down, we hook up `ParaT.Initialize` such that its invoked after trame server
        # is ready.
        self._app = ParaT()
        # This flag is used in `monitor_server_status` that increments the server progress
        # circle as long as app is running.
        self._running = True
        # A change in `state` may trigger UI callbacks. This flag is used to guard sensitive
        # code that should not execute until this app's `on_server_ready` hook has executed.
        self._ready = False

        # List of proxies whose output representation are hidden in the render view.
        self.hidden_pipeline_proxy_ids = []
        # Keep track of active proxy with the help of the `ActiveObjects` microservice.
        self.active_proxy = None
        # Assigned to the most recently created representation.
        self.active_representation = None
        # Assigned to the most recently created view. Since this application uses a single view,
        # `active_view` is also used to get a handle to the view proxy.
        self.active_view = None

        # `target_fps` caps the rate of render requests per second initiated by user interaction or
        # the camera spin.
        self.state.target_fps = INITIAL_TARGET_FPS
        # `readable_stream_bitrate` displays a human readable value with proper units `B/s`, `KB/s`, `MB/s`
        # of the current stream bitrate next to the slider. Only applicable for video encoders.
        self.state.readable_stream_bitrate = get_human_readable_stream_bitrate(
            INITIAL_STREAM_BITRATE * 1000
        )
        # Proxy parameters.
        # These variables represent the shared state between the web server and the trame application.
        # Any updates to these will reflect in the UI and vice versa.
        self.state.wavelet_size = (10, 10, 10)
        self.state.contours_count = 10
        self.state.working_proxy = ""
        # Global ID of proxies.
        self.state.wavelet_id = 0
        self.state.contour_id = 0
        self.state.clip_id = 0
        self.state.threshold_id = 0

        # Register callbacks that initialize `ParaT` after the server is ready and
        # clean up `vtkPVApplication` after the server exits.
        self.ctrl.on_server_ready.add_task(self.initialize)
        self.ctrl.on_server_exited.add_task(self.finalize)

        # React to changes in application state. All of these callbacks utilize
        # the `PropertyManager` microservice to update and push relevant proxy
        # properties to server side objects.
        self.state.change("contours_count")(self.ui_state_contours_update)
        self.state.change("wavelet_size")(self.ui_state_wavelet_update)
        self.state.change("clip_x_origin")(self.ui_state_clip_update)
        self.state.change("slice_x_origin")(self.ui_state_slice_update)
        self.state.change("stream_encoder")(self.ui_state_encoder_update)
        self.state.change("threshold_range")(self.ui_state_threshold_update)
        self.state.change("target_fps")(self.ui_state_target_fps_change)
        self.state.change("stream_quality_range")(
            self.ui_state_stream_quality_range_change
        )
        self.state.change("stream_bitrate")(self.ui_state_stream_bitrate_change)

    # ---------------------------------------------------------------
    # Instance life cycle
    # ---------------------------------------------------------------

    async def initialize(self, **kwargs):
        """Initialize `ParaT` application and setup important microservices.
        """
        self._session = await self._app.initialize()
        self._pipeline = PipelineViewer(self._session)
        self._builder = PipelineBuilder(self._session)
        self._def_mgt = DefinitionManager(self._session)
        self._active = ActiveObjects(self._session, "ActiveSources")
        self._prop_mgr = PropertyManager()
        self._progress = ProgressObserver(self._session)
        self._apply_ctrl = ApplyController(self._session)

        # default demo pipeline
        await self.setup_demo()

        # Tasks to monitor state change
        asynchronous.create_task(self.monitor_active_proxy())
        asynchronous.create_task(self.monitor_apply())
        asynchronous.create_task(self.monitor_pipeline_update())
        asynchronous.create_task(self.monitor_progress_ds())
        asynchronous.create_task(self.monitor_progress_rs())
        asynchronous.create_task(self.monitor_server_status())

        # RemoteControllerArea
        self._view_handler = ViewAdapter(self.active_view, "view")
        self.ctrl.rc_area_register(self._view_handler)

        self._ready = True

    async def finalize(self, **kwargs):
        """Finalize the application. This releases resources consumed by the async paraview library.
        """
        await self._app.finalize()

    # ---------------------------------------------------------------
    # Background async monitoring tasks
    # ---------------------------------------------------------------

    async def monitor_active_proxy(self):
        """Update the data information and git tree when active proxy changes.
        """
        async for proxy in self._active.GetCurrentObservable():
            self.active_proxy = proxy
            active_ids = []
            data_info = {}

            if proxy:
                active_ids = [str(proxy.GetGlobalID())]
                info = proxy.GetDataInformation(0)
                data_info = dict(
                    points=info.GetNumberOfPoints(), cells=info.GetNumberOfCells()
                )

            with self.state as state:
                state.git_tree_actives = active_ids
                state.active_data_info = data_info

    async def monitor_apply(self):
        """Update the data information when a pipeline apply event occurs.
        """
        async for apply_event in self._apply_ctrl.GetObservable():
            if self.active_proxy:
                await self._prop_mgr.UpdatePipeline(self.active_proxy)
                with self.state as state:
                    info = self.active_proxy.GetDataInformation(0)
                    state.active_data_info = dict(
                        points=info.GetNumberOfPoints(), cells=info.GetNumberOfCells()
                    )

    async def monitor_pipeline_update(self):
        """Update the pipeline tree UI when the paraview pipeline changes.
        """
        async for pipelineState in self._pipeline.GetObservable():
            self._push_pipeline_to_ui(pipelineState)

    async def monitor_progress_ds(self):
        """Display amount of data service progress and progress message.
        """
        async for message in self._progress.GetServiceProgressObservable("ds"):
            data = json.loads(message)
            message = data.get("ds", {})
            progress = message.get("Progress", 100)

            with self.state as state:
                state.status_ds_idle = progress == 100
                state.status_ds_progress = progress
                if state.status_ds_idle:
                    self.state.working_proxy = ""
                else:
                    self.state.working_proxy = message.get("Message", "")

    async def monitor_progress_rs(self):
        """Display amount of render service progress and progress message.
        """
        async for message in self._progress.GetServiceProgressObservable("rs"):
            data = json.loads(message)
            progress = data.get("rs", {}).get("Progress", 100)
            self.state.status_rs_idle = progress == 100

    async def monitor_server_status(self):
        """Display server activity and spin the view's camera if spinning=True
        """
        while self._running:
            with self.state as state:
                await asyncio.sleep(1 / self.state.target_fps)

                # Spinning
                if state.spinning and self.active_view:
                    self.active_view.GetCamera().Azimuth(1)
                    self.active_view.InteractiveRender()

                # Update client
                state.status_server += 5
                if state.status_server > 360:
                    state.status_server = 0

    # ---------------------------------------------------------------
    # General API
    # ---------------------------------------------------------------

    async def create_proxy(self):
        """Create a proxy using the `PipelineBuilder` microservice.
        """
        with self.state as state:
            xml_group = state.xml_group
            xml_name = state.xml_name
            proxy = None
            if xml_group == "sources":
                proxy = await self._builder.CreateProxy(xml_group, xml_name)
            elif xml_group == "filters":
                input = self.active_proxy
                proxy = await self._builder.CreateProxy(
                    xml_group, xml_name, Input=input
                )
            elif xml_group == "representations":
                input = self.active_proxy
                view = self.active_view
                proxy = await self._builder.CreateRepresentation(
                    input, 0, view, xml_name
                )
                self.active_representation = proxy
            elif xml_group == "views":
                input = self.active_proxy
                proxy = await self._builder.CreateProxy(xml_group, xml_name)
                self.active_view = proxy
            else:
                print(f"Not sure what to create with {xml_group}::{xml_name}")

            # No proxy just skip work...
            if proxy is None:
                print("!!! No proxy created !!!")
                return

    async def setup_demo(self):
        """Setup paraview pipeline with a wavelet source, contour, clip, slice and threshold filters,
        corresponding representations and a view.
        """
        view = await self._builder.CreateProxy("views", "RenderView")
        hide_list = []

        # wavelet ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        # creating a representation before Apply() we avoid the default type
        # which is volume representation
        self.wavelet = await self._builder.CreateProxy("sources", "RTAnalyticSource")
        rep = await self._builder.CreateRepresentation(
            self.wavelet,
            0,
            view,
            "GeometryRepresentation",
        )
        self._prop_mgr.SetValues(rep, Representation="Outline")
        self._prop_mgr.Push(self.wavelet, rep)

        # Contour ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        self.contour = await self._builder.CreateProxy(
            "filters",
            "Contour",
            Input=self.wavelet,
            ContourValues=generate_contour_values(WAVELET_SCALAR_RANGE, 10),
            SelectInputScalars=["", "", "", "", "RTData"],
        )
        rep = await self._builder.CreateRepresentation(
            self.contour, 0, view, "GeometryRepresentation"
        )
        self._prop_mgr.Push(self.contour, rep)
        hide_list.append(rep)

        # Clip ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        self.clip = await self._builder.CreateProxy(
            "filters",
            "Clip",
            Input=self.contour,
            PreserveInputCells=True,  # aka Crinkle clip
        )
        rep = await self._builder.CreateRepresentation(
            self.clip, 0, view, "GeometryRepresentation"
        )
        self._prop_mgr.Push(self.clip, rep)

        # Slice ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        self.slice = await self._builder.CreateProxy(
            "filters",
            "Cut",
            Input=self.wavelet,
        )
        cutFunction = self._prop_mgr.GetValues(self.slice)["CutFunction"]
        self._prop_mgr.SetValues(cutFunction, force_push=True, Normal=[0, 1, 0])
        self._prop_mgr.SetValues(self.slice, CutFunction=cutFunction)

        rep = await self._builder.CreateRepresentation(
            self.slice, 0, view, "GeometryRepresentation"
        )
        self._prop_mgr.Push(self.slice, rep)

        # Threshold ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        self.threshold = await self._builder.CreateProxy(
            "filters",
            "Threshold",
            Input=self.wavelet,
            SelectInputScalars=["", "", "", "", "RTData"],
            LowerThreshold=180,
            UpperThreshold=240,
        )
        rep = await self._builder.CreateRepresentation(
            self.threshold, 0, view, "GeometryRepresentation"
        )
        self._prop_mgr.Push(self.threshold, rep)

        # Plane ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        # create a plane source as a fake widget of the clip
        # make sure to keep them synchronized
        self.plane = await self._builder.CreateProxy(
            "sources",
            "PlaneSource",
            Origin=[0, -10, -10],
            Point1=[0, -10, 10],
            Point2=[0, 10, -10],
            XResolution=10,
            YResolution=10,
        )
        rep = await self._builder.CreateRepresentation(
            self.plane, 0, view, "GeometryRepresentation"
        )
        self._prop_mgr.SetValues(rep, Representation="Wireframe")
        self._prop_mgr.Push(self.plane, rep)
        self.hidden_pipeline_proxy_ids.append(self.plane.GetGlobalID())

        # View encoding setup
        self._prop_mgr.SetValues(
            view,
            force_push=True,
            LosslessMode=False,
            JpegMode=True,
            StillStreamQuality=INITIAL_STILL_STREAM_QUALITY,
            InteractiveStreamQuality=INITIAL_INTERACTIVE_STREAM_QUALITY,
            TargetBitrate=INITIAL_STREAM_BITRATE,
            Display=False,
            StreamOutput=True,
        )
        # Push everything after view is setup
        self._apply_ctrl.Apply()
        # ideally we would like to wait on the internal update of Apply()
        await self._prop_mgr.Update(view)

        # Visibility handling (need to happen after apply)
        for rep in hide_list:
            self._prop_mgr.SetValues(
                rep,
                force_push=True,
                Visibility=0,
            )

        # Keep track of view and ids
        self.active_view = view
        self.state.wavelet_id = str(self.wavelet.GetGlobalID())
        self.state.contour_id = str(self.contour.GetGlobalID())
        self.state.clip_id = str(self.clip.GetGlobalID())
        self.state.slice_id = str(self.slice.GetGlobalID())
        self.state.threshold_id = str(self.threshold.GetGlobalID())

        # Activate wavelet by default
        self._active.SetCurrentProxy(self.wavelet, vtkSMProxySelectionModel.CLEAR)

    def _push_pipeline_to_ui(self, pipelineState):
        list_to_fill = []
        for item in pipelineState:
            rep_id = item.GetRepresentationIDs(self.active_view)[0]
            rep = self._session.GetProxyManager().FindProxy(rep_id)
            visible = self._prop_mgr.GetValues(rep)["Visibility"] if rep else False
            node = {
                "name": item.GetName(),
                "parent": str(
                    item.GetParentIDs()[0] if len(item.GetParentIDs()) > 0 else 0
                ),
                "id": str(item.GetID()),
                "visible": visible,
            }
            if item.GetID() not in self.hidden_pipeline_proxy_ids:
                list_to_fill.append(node)

        with self.state as state:
            state.git_tree_sources = list_to_fill

    # ---------------------------------------------------------------
    # GUI callbacks
    # ---------------------------------------------------------------

    def ui_event_pipeline_update(self, active):
        proxy = self._session.GetProxyManager().FindProxy(int(active[0]))
        self._active.SetCurrentProxy(proxy, vtkSMProxySelectionModel.CLEAR)

    def ui_event_pipeline_visibility_update(self, selection):
        proxy_id = selection.get("id")
        visible = selection.get("visible")
        proxy = self._session.GetProxyManager().FindProxy(int(proxy_id))
        rep = self.active_view.FindRepresentation(proxy, 0)
        self._prop_mgr.SetValues(rep, force_push=True, Visibility=visible)

        # for clip update also the visibility of the "widget"
        if proxy == self.clip:
            rep = self.active_view.FindRepresentation(self.plane, 0)
            self._prop_mgr.SetValues(rep, force_push=True, Visibility=visible)

        self._apply_ctrl.Apply()

        pipelineState = self._pipeline.GetCurrentState()
        self._push_pipeline_to_ui(pipelineState)

    def ui_state_contours_update(self, contours_count, **kwargs):
        if not self._ready:
            return

        self._prop_mgr.SetValues(
            self.contour,
            force_push=True,
            ContourValues=generate_contour_values(WAVELET_SCALAR_RANGE, contours_count),
        )
        self._apply_ctrl.Apply()

    def ui_state_wavelet_update(self, wavelet_size, **kwargs):
        if not self._ready:
            return

        x, y, z = wavelet_size
        self._prop_mgr.SetValues(
            self.wavelet,
            force_push=True,
            WholeExtent=[-x, x, -y, y, -z, z],
        )

        # update plane
        clipFunction = self._prop_mgr.GetValues(self.clip)["ClipFunction"]
        scaled_offset = self._prop_mgr.GetValues(clipFunction)["Offset"]
        self._prop_mgr.SetValues(
            self.plane,
            force_push=True,
            Origin=[scaled_offset, -y, -z],
            Point1=[scaled_offset, -y, z],
            Point2=[scaled_offset, y, -z],
        )
        self._apply_ctrl.Apply()

    def ui_state_clip_update(self, clip_x_origin, **kwargs):
        if not self._ready:
            return

        minX, maxX, minY, maxY, minZ, maxZ = self._prop_mgr.GetValues(self.wavelet)[
            "WholeExtent"
        ]
        scaled_offset = (maxX - minX) * clip_x_origin / 2.0

        # update plane first
        self._prop_mgr.SetValues(
            self.plane,
            force_push=True,
            Origin=[scaled_offset, minY, minZ],
            Point1=[scaled_offset, minY, maxZ],
            Point2=[scaled_offset, maxY, minZ],
        )

        clipFunction = self._prop_mgr.GetValues(self.clip)["ClipFunction"]
        self._prop_mgr.SetValues(clipFunction, force_push=True, Offset=scaled_offset)
        # see  async/paraview#136
        self._prop_mgr.SetValues(self.clip, ClipFunction=clipFunction, force_push=True)
        self.clip.GetProperty("ClipFunction").Modified()

        self._apply_ctrl.Apply()

    def ui_state_slice_update(self, slice_x_origin, **kwargs):
        if not self._ready:
            return

        minX, maxX, minY, maxY, minZ, maxZ = self._prop_mgr.GetValues(self.wavelet)[
            "WholeExtent"
        ]
        scaled_offset = (maxX - minX) * slice_x_origin / 2.0

        cutFunction = self._prop_mgr.GetValues(self.slice)["CutFunction"]
        self._prop_mgr.SetValues(cutFunction, force_push=True, Offset=scaled_offset)
        # see  async/paraview#136
        self._prop_mgr.SetValues(self.slice, CutFunction=cutFunction, force_push=True)
        self.slice.GetProperty("CutFunction").Modified()

        self._apply_ctrl.Apply()

    def ui_state_threshold_update(self, threshold_range, **kwargs):
        if not self._ready:
            return
        self._prop_mgr.SetValues(
            self.threshold,
            force_push=True,
            LowerThreshold=threshold_range[0],
            UpperThreshold=threshold_range[1],
        )
        self._apply_ctrl.Apply()

    def ui_state_target_fps_change(self, target_fps, **kwargs):
        if not self._ready:
            return
        self._view_handler.target_fps = target_fps

    def ui_state_encoder_update(self, stream_encoder, **kwargs):
        if not self._ready:
            return

        if stream_encoder == "RGB":
            self._prop_mgr.SetValues(
                self.active_view,
                force_push=True,
                JpegMode=False,
                LosslessMode=True,
                Display=False,
                StreamOutput=True,
                LosslessPixelFormat=1,  # VTKPixelFormatType::RGB24
                LosslessPictureSliceOrder=0,  # vtkRawVideoFrame::SliceOrder::TopDown
            )
            self.state.active_display_mode = "raw-image"

        if stream_encoder == "RGBA":
            self._prop_mgr.SetValues(
                self.active_view,
                force_push=True,
                JpegMode=False,
                LosslessMode=True,
                Display=False,
                StreamOutput=True,
                LosslessPixelFormat=0,  # VTKPixelFormatType::RGBA32
                LosslessPictureSliceOrder=0,  # vtkRawVideoFrame::SliceOrder::TopDown
            )
            self.state.active_display_mode = "raw-image"

        if stream_encoder == "JPG":
            self._prop_mgr.SetValues(
                self.active_view,
                force_push=True,
                JpegMode=True,
                LosslessMode=False,
                Display=False,
                StreamOutput=True,
            )
            self.state.active_display_mode = "image"

        # VP9 is the default, so, there is no need to acquire the encoder proxy and set codec type.
        if stream_encoder == "VP9_SW":
            print("Use VP9 with software encoder")
            self._prop_mgr.SetValues(
                self.active_view,
                force_push=True,
                JpegMode=False,
                LosslessMode=False,
                Display=False,
                StreamOutput=True,
                VideoEncoderType="VpxEncoder",
                MediaSegmentsMode=False,
                RequestMediaInitializationSegment=False,
            )
            self.state.active_display_mode = "video-decoder"

        if stream_encoder == "H264_NVENC":
            print("Use H264 with NVENC(Nvidia) hardware encoder")
            self._prop_mgr.SetValues(
                self.active_view,
                force_push=True,
                JpegMode=False,
                LosslessMode=False,
                Display=False,
                StreamOutput=True,
                VideoEncoderType="NVIDIAEncoder",
                MediaSegmentsMode=False,
                RequestMediaInitializationSegment=False,
            )
            self.state.active_display_mode = "video-decoder"

        if stream_encoder == "VP9_SW_WEBM":
            print("Use VP9_WEBM with software encoder")
            self._prop_mgr.SetValues(
                self.active_view,
                force_push=True,
                JpegMode=False,
                LosslessMode=False,
                Display=False,
                StreamOutput=True,
                VideoEncoderType="VpxEncoder",
                MediaSegmentsMode=True,
                RequestMediaInitializationSegment=True,
            )
            self.state.active_display_mode = "media-source"

        self.active_view.StillRender()

    def ui_state_stream_quality_range_change(self, stream_quality_range, **kwargs):
        if not self._ready:
            return

        self._prop_mgr.SetValues(
            self.active_view,
            force_push=True,
            InteractiveStreamQuality=stream_quality_range[0],
            StillStreamQuality=stream_quality_range[1],
        )

    def ui_state_stream_bitrate_change(self, stream_bitrate, **kwargs):
        if not self._ready:
            return

        self._prop_mgr.SetValues(
            self.active_view,
            force_push=True,
            TargetBitrate=stream_bitrate,
        )

        value = stream_bitrate * 1000
        self.state.readable_stream_bitrate = get_human_readable_stream_bitrate(value)

    def reset_target_fps(self):
        if not self._ready:
            return

        self.state.target_fps = INITIAL_TARGET_FPS

    def reset_stream_bitrate(self):
        if not self._ready:
            return

        self.state.stream_bitrate = INITIAL_STREAM_BITRATE
        value = INITIAL_STREAM_BITRATE * 1000
        self.state.readable_stream_bitrate = get_human_readable_stream_bitrate(value)

    def reset_stream_quality(self):
        if not self._ready:
            return

        self.state.stream_quality_range = (INITIAL_INTERACTIVE_STREAM_QUALITY, INITIAL_STILL_STREAM_QUALITY)


# -----------------------------------------------------------------------------
# Setup
# -----------------------------------------------------------------------------

server = get_server()
app = App(server)

# -----------------------------------------------------------------------------
# GUI
# -----------------------------------------------------------------------------

with SinglePageWithDrawerLayout(server) as layout:
    with layout.icon:
        vuetify.VIcon("mdi-clock-fast")

    with layout.title as title:
        title.style = "padding-left: 0;"
        title.set_text("Async ParaView")

    with layout.toolbar as toolbar:
        toolbar.dense = True
        vuetify.VSpacer()
        html.Div("{{ working_proxy }}", classes="text-subtitle-1")
        vuetify.VProgressCircular(
            "D",
            color="amber",
            size=35,
            width=5,
            indeterminate=("status_ds_idle", True),
            value=("status_ds_progress", 20),
            classes="mx-2",
        )
        vuetify.VProgressCircular(
            "R",
            color="purple",
            size=35,
            width=5,
            indeterminate=("status_rs_idle", True),
            rotate=("status_server", 0),
            value=("20",),
            classes="mx-2",
        )
        vuetify.VProgressCircular(
            "S",
            color="red",
            size=35,
            width=5,
            rotate=("status_server", 0),
            value=("20",),
            classes="mx-2",
        )
        vuetify.VProgressCircular(
            "C",
            color="teal",
            size=35,
            width=5,
            indeterminate=True,
            classes="mx-2",
        )
        vuetify.VDivider(vertical=True, classes="mx-2")
        vuetify.VCheckbox(
            small=True,
            v_model=("spinning", False),
            dense=True,
            classes="mx-2",
            hide_details=True,
            color="success",
            on_icon="mdi-axis-z-rotate-counterclockwise",
            off_icon="mdi-axis-z-rotate-counterclockwise",
        )
        with vuetify.VBtn(
            icon=True, small=True, click=app.ctrl.view_reset_camera, classes="mx-2"
        ):
            vuetify.VIcon("mdi-crop-free")

    with layout.drawer as drawer:
        drawer.width = 300
        trame.GitTree(
            sources=("git_tree_sources", []),
            actives=("git_tree_actives", []),
            visibility_change=(app.ui_event_pipeline_visibility_update, "[$event]"),
            actives_change=(app.ui_event_pipeline_update, "[$event]"),
        )
        with vuetify.VCard(
            classes="mb-2 mx-1", v_show="git_tree_actives.includes(wavelet_id)"
        ):
            with vuetify.VCardTitle(classes="py-0"):
                html.Div("Wavelet")
                vuetify.VSpacer()
                html.Div("{{ wavelet_size }}")

            vuetify.VDivider()
            with vuetify.VCardText():
                vuetify.VSlider(
                    v_model=("wavelet_size[0]",),
                    input=(app.ui_state_wavelet_update, "[wavelet_size]"),
                    min=5,
                    max=200,
                    step=20,
                    hide_details=True,
                    dense=True,
                )
            with vuetify.VCardText():
                vuetify.VSlider(
                    v_model=("wavelet_size[1]",),
                    input=(app.ui_state_wavelet_update, "[wavelet_size]"),
                    min=5,
                    max=200,
                    step=20,
                    hide_details=True,
                    dense=True,
                )
            with vuetify.VCardText():
                vuetify.VSlider(
                    v_model=("wavelet_size[2]",),
                    input=(app.ui_state_wavelet_update, "[wavelet_size]"),
                    min=5,
                    max=200,
                    step=20,
                    hide_details=True,
                    dense=True,
                )
        with vuetify.VCard(
            classes="mb-2 mx-1", v_show="git_tree_actives.includes(contour_id)"
        ):
            with vuetify.VCardTitle(classes="py-0"):
                html.Div("Contours")
                vuetify.VSpacer()
                html.Div("{{ contours_count }}")

            vuetify.VDivider()
            with vuetify.VCardText():
                vuetify.VSlider(
                    v_model=("contours_count", 10),
                    min=5,
                    max=100,
                    step=10,
                    hide_details=True,
                    dense=True,
                )
        with vuetify.VCard(
            classes="mb-2 mx-1", v_show="git_tree_actives.includes(clip_id)"
        ):
            with vuetify.VCardTitle(classes="py-0"):
                html.Div("Clip")
                vuetify.VSpacer()
                html.Div("{{ clip_x_origin }}")

            vuetify.VDivider()
            with vuetify.VCardText():
                vuetify.VSlider(
                    v_model=("clip_x_origin", 0),
                    min=-1,
                    max=1,
                    step=0.1,
                    hide_details=True,
                    dense=True,
                )
        with vuetify.VCard(
            classes="mb-2 mx-1", v_show="git_tree_actives.includes(slice_id)"
        ):
            with vuetify.VCardTitle(classes="py-0"):
                html.Div("Slice")
                vuetify.VSpacer()
                html.Div("{{ slice_x_origin }}")

            vuetify.VDivider()
            with vuetify.VCardText():
                vuetify.VSlider(
                    v_model=("slice_x_origin", 0),
                    min=-1,
                    max=1,
                    step=0.1,
                    hide_details=True,
                    dense=True,
                )
        with vuetify.VCard(
            classes="mb-2 mx-1", v_show="git_tree_actives.includes(threshold_id)"
        ):
            with vuetify.VCardTitle(classes="py-0"):
                html.Div("Threshold")
                vuetify.VSpacer()
                html.Div("{{ threshold_range }}")
            vuetify.VDivider()
            with vuetify.VCardText():
                vuetify.VRangeSlider(
                    v_model=("threshold_range", (180, 240)),
                    min=WAVELET_SCALAR_RANGE[0],
                    max=WAVELET_SCALAR_RANGE[1],
                    step=0.5,
                    hide_details=True,
                    dense=True,
                )

        with vuetify.VCard(classes="mb-2 mx-1"):
            with vuetify.VCardTitle(classes="py-0"):
                html.Div("Stats")
                vuetify.VSpacer()
                vuetify.VIcon("mdi-dots-triangle", x_small=True, classes="mr-1")
                html.Div(
                    "{{ active_data_info?.points.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',') | 0 }}",
                    classes="text-caption",
                )
                vuetify.VSpacer()
                vuetify.VIcon("mdi-triangle-outline", x_small=True, classes="mr-1")
                html.Div(
                    "{{ active_data_info?.cells.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',') || 0 }}",
                    classes="text-caption",
                )
            vuetify.VDivider()
            with vuetify.VCardText(style="height: 150px"):
                rca.StatisticsDisplay(
                    name="view",
                    fps_delta=1.5,
                    stat_window_size=10,
                    history_window_size=30,
                    reset_ms_threshold=100,
                )

        with vuetify.VCard(classes="my-2 mx-1"):
            with vuetify.VCardTitle(classes="py-0"):
                html.Div("Image Delivery")
            vuetify.VDivider()
            with vuetify.VCardText():
                vuetify.VSelect(
                    label="Encoder",
                    v_model=("stream_encoder", "JPG"),
                    items=(
                        "['RGB', 'RGBA', 'JPG', 'VP9_SW', 'H264_NVENC', 'VP9_SW_WEBM']",
                    ),
                    hide_details=True,
                    dense=True,
                )
                html.Div("Target: {{ target_fps }} fps", classes="text-subtitle-2 mt-4")
                with vuetify.VRow():
                    vuetify.VSlider(
                        v_model=("target_fps", 30),
                        min=10,
                        max=60,
                        step=5,
                        hide_details=True,
                        dense=True,
                    )
                    with vuetify.VBtn(
                        icon=True, small=True, click=app.reset_target_fps, classes="mx-2"
                    ):
                        vuetify.VIcon("mdi-restore")
                with html.Div(
                    v_show="active_display_mode != 'image' && active_display_mode != 'raw-image'"
                ):
                    html.Div(
                        "Target bitrate: {{ readable_stream_bitrate }}",
                        classes="text-subtitle-2 mt-4",
                    )
                    with vuetify.VRow():
                        vuetify.VSlider(
                            v_model=("stream_bitrate", INITIAL_STREAM_BITRATE),
                            min=100,
                            max=16000,
                            step=400,
                            hide_details=True,
                            dense=True,
                        )
                        with vuetify.VBtn(
                            icon=True, small=True, click=app.reset_stream_bitrate, classes="mx-2"
                        ):
                            vuetify.VIcon("mdi-restore")
                with html.Div(v_show="active_display_mode != 'raw-image'"):
                    html.Div(
                        "Stream quality: {{ stream_quality_range }}",
                        classes="text-subtitle-2 mt-4",
                    )
                    with vuetify.VRow():
                        vuetify.VRangeSlider(
                            v_model=("stream_quality_range", (INITIAL_INTERACTIVE_STREAM_QUALITY, INITIAL_STILL_STREAM_QUALITY)),
                            min=0,
                            max=100,
                            step=1,
                            hide_details=True,
                            dense=True,
                        )
                        with vuetify.VBtn(
                            icon=True, small=True, left=False,  click=app.reset_stream_quality, classes="mx-2"
                        ):
                            vuetify.VIcon("mdi-restore")

    with layout.content:
        with vuetify.VContainer(fluid=True, classes="pa-0 fill-height"):
            rca.RemoteControlledArea(
                name="view", display=("active_display_mode", "image")
            )

# -----------------------------------------------------------------------------
# CLI
# -----------------------------------------------------------------------------

if __name__ == "__main__":
    server.start()
